# Flughafendb for Postgres

This is a postgres conversion of the [Flughafendb](https://github.com/stefanproell/flughafendb). The original is made for MySQL so I converted it for the use with PostgreSQL.

## Installation / Import
You can import the script with the postgres-commandline-client.
```
zcat flughafendb_postgres.sql.gz | psql -u postgres -p flughafendb
```

## Versions
The file `flughafendb_postgres.sql.gz` uses the copy for faster imports.  
The file `flughafendb_inserts_postgres.sql.gz` uses single insert-statements for better compatibility with older versions of postgres. But it will be slower.